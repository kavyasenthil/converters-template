# Create softwareserver for polyglot
FROM ncsapolyglot/polyglot:develop
MAINTAINER Rob Kooper <kooper@illinois.edu>

# Install requirements, enable shellscripts to be scanned, enable imagemagick conversion by adding to .aliases.txt
USER root
RUN apt-get update && apt-get -y install vim nano git python imagemagick && \
	/bin/sed -i -e 's/^\([^#]*Scripts=\)/#\1/' -e 's/^#\(ShellScripts=\)/\1/' /home/polyglot/polyglot/SoftwareServer.conf && \
	/bin/sed -i -e 's#RabbitMQURI=amqp://guest:guest@localhost:5672/%2F#RabbitMQURI=amqp://guest:guest@rabbitmq:5672/%2F#' /home/polyglot/polyglot/SoftwareServerRestlet.conf && \
	echo "ImageMagick" > /home/polyglot/polyglot/scripts/sh/.aliases.txt

# Copy convert file to scripts/sh folder in container (this is done to keep cache so you can debug script easily)
COPY ImageMagick_convert.sh /home/polyglot/polyglot/scripts/sh/
RUN chown polyglot /home/polyglot/polyglot/scripts/sh/ImageMagick_convert.sh && \
    chmod +x /home/polyglot/polyglot/scripts/sh/ImageMagick_convert.sh

# Back to polyglot
CMD ["softwareserver"]
