# Brown Dog Converter Template

This README provides step-by-step instructions to create a Brown Dog converter, starting with a working example project. Once you have the the example running, you may customize it, test it, and contribute your new converter to the Brown Dog Tools Catalog.

#### Step 1. Set up a Docker runtime environment

- Install Docker. The steps will vary by operating system:
    - For Mac OS X, follow the step-by-step instructions provided in the link: https://docs.docker.com/mac/step_one/
     - The steps work even if you have pre-installed virtualbox. Make sure that you close virtual box before starting the installation procedure.
     - You do not need superuser/root privileges to install Docker.
     - To enter docker commands, click on the Quickstart Terminal icon in the Application/Docker folder.
     - Commands for the creation, exectuion, and deletion of Docker images/containers using the terminal can be found here: https://docs.docker.com/engine/installation/mac/
    - For Linux, add the appropriate docker package repository to install the latest Docker version. For example, see Ubuntu instructions: https://docs.docker.com/engine/installation/linux/ubuntulinux/

- Verify the installation by running the hello-world container.
    ```
    docker run hello-world
    ```
    - Mac NOTE: If Quickstart Terminal startup script did not configure the shell correctly, you may get message “docker: cannot connect to the Docker daemon. Is the docker daemon running on this host?.”
     - Use the following commands to configure the shell:
    ```
    docker-machine env default
    eval $(docker-machine env default)
    ```
     - Now, you should be able to run hello-world container.
    -  Some common troubleshooting for installation can be found here : https://docs.docker.com/faqs/troubleshoot

#### Step 2. Obtain template project from NCSA repository

- In your terminal, change to the folder where you want to create your project.
- Clone the git repository into a new project folder. Substitute your project name for <project name> below:
```
git clone https://opensource.ncsa.illinois.edu/bitbucket/scm/bd/converters-template.git <project name>
```
- The template directory consists of four files: this README.md, docker-compose.yml, Dockerfile, and ImageMagick_convert.sh.

#### Step 3: Build a Docker image for the converter

- This command creates a Docker container image for the example converter:
```
   docker build -t converters-template .
```

#### Step 4: Launch the runtime environment

- Run the docker-compose.yml:
   ```
   docker-compose up
   ```
- Docker Compose obtains the ncsa/polyglot-server image, mongodb image, and rabbitmq from docker Hub and launches these services within different containers. It also starts the example converter that you built. The containers are configured with the appropriate network ports linking them together.

#### Step 4a: Check file permissions (TODO: This is not necessary on linux)

- Open another docker terminal
    + Open Kitematic, press button 'DOCKER CLI' in bottom-left corner
- Make sure the permission for ImageMagick_convert.sh is 755.
   ```
   $ chmod 755 ImageMagick_convert.sh
   ```

#### Step 5: Testing the example converter

- Polyglot server will be running at http://\<docker-machine-ip\>:9000 where \<docker-machine-ip\> can be found by running the following command.
This assumes that you are running a single Docker machine. If not, please choose the appropriate IP address:
   ```
   $ docker-machine ip
   ```
- In a browser, visit http://\<docker-machine-ip\>:8184. You should be able to see the test endpoints of polyglot server.
- If you have the cURL command, you can test the example (image conversion) directly. Have cURL POST a request to the /convert endpoint, for example:
```
curl -F "File=@samples/test.jpg" \<docker-machine-ip\>:8184/convert/png
```

This returns a page with a link to the converted file. The link may give a 404 at first, but it will return the resulting image after your converter has had a chance to run.

#### Step 6: Modifying the template and creating your own converter

- Rename ImageMagick_convert.sh:
   ```
   mv ImageMagick_convert.sh MyTool_convert.sh
   ```
- Modify the MyTool_convert.sh using any editor (e.g. vim). A simple example is shown below:
   ```
   #!/bin/sh
   #ImageMagick (v6.5.2)
   #image
   #png bmp foobar
   #jpg

   output_filename=$(basename "$2")
   output_format="${output_filename##*.}"

   #Output PGM files as ASCII
   if [ "$output_format" = "pgm" ]; then
      convert "$1" -compress none "$2"
   else
      convert "$1" "$2"
   fi
   ```

- Change the above code
    - Line 2 : Change ImageMagick to MyTool
    - Line 4 :  Replace foobar with gif
    - Line 5:  Add ico
    - Then save the file.
    - A detail document explaining how to write converter for any tool can be found here:
    - https://opensource.ncsa.illinois.edu/confluence/display/BD/Adding+Conversions+to+the+DAP+and+Calling+the+DAP
- Modify Dockerfile file using any editor (e.g. vim)
   ```
   1 # Create softwareserver for polyglot.
   2 FROM ncsa/polyglot-server:latest
   3 MAINTAINER Rob Kooper <kooper@illinois.edu>
   4
   5 USER root
   6 # - install requirements
   7 # - enable shellscripts to be scanned
   8 # - enable imagemagick conversion by adding to .aliases.txt
   9 RUN apt-get update && apt-get -y install imagemagick && \
   10 /bin/sed -i -e 's/^\([^#]*Scripts=\)/#\1/' -e 's/^#\(ShellScripts=\)/\1/'    /home/polyglot/polyglot/SoftwareServer.conf && \
   11 echo "ImageMagick" > /home/polyglot/polyglot/scripts/sh/.aliases.txt
   12
   13 # copy convert file to scripts/sh folder in container
   14 # this is done to keep cache so you can debug script easily
   15 COPY ImageMagick_convert.sh /home/polyglot/polyglot/scripts/sh/
   16
   17 # back to polyglot
   18 USER polyglot
   19 CMD ["softwareserver"]
   ```
- Line 11:  Add MyTool to the .aliases.txt
   - Modify:
      ```
      echo "ImageMagick" > /home/polyglot/polyglot/scripts/sh/.aliases.txt
      ```
  - To:
      ```
      echo "MyTool" > /home/polyglot/polyglot/scripts/sh/.aliases.txt
      ```
- Line 15: Copy MyTool_convert.sh script to the script directory
     ```
     COPY MyTool_convert.sh /home/polyglot/polyglot/scripts/sh/
     ```
- Save the File
- To build, launch and test the new converter, run the following command:
   ```
   docker-compose stop
   docker build –t mytool .
   docker-compose up
   ```
